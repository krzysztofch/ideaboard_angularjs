<?php

class Update {
	public $success;
	public $error;
	public $message;

	public function __construct($bool) {
		if($bool == true) {
			$this->success = true;
			$this->message = "Zapisano!";
		} else {
			$this->success = false;
			$this->message = "Nie udało się zmienić hasła!";		
		}
	}
}