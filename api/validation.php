<?php

class Validation {
	public $success;
	public $errorName;
	public $errorEmail;
	public $errorPass;
	public $errorPassconf;

	public function __construct($bool, $errorName, $errorEmail, $errorPass, $errorPassconf) {
		if($bool == true) {
			$this->success = true;
			$this->errorName = "";
			$this->errorEmail = "";
			$this->errorPass = "";
			$this->errorPassconf ="";
		} else {
			$this->success = false;
			$this->errorName = $errorName;
			$this->errorEmail = $errorEmail;
			$this->errorPass = $errorPass;
			$this->errorPassconf = $errorPassconf;			
		}
	}
}