<?php

require_once('init.php');

$id = $_GET['id'];
$id = isset($_GET['id']) ? $_GET['id'] : die();
$table = $_GET['tableNumber'];

switch($table) {
	case 0:
		$stmtToDo = $Database->query("SELECT id, text, reporter FROM todo WHERE id='$id' ");
		$resultToDo = array();
		while($row = $stmtToDo->fetch_assoc())
		{
			array_push($resultToDo, $row);
		}
		$oneMessage = array();
		array_push($oneMessage, $resultToDo);
		break;
	case 1:
		$stmtToImprove = $Database->query("SELECT id, text, reporter FROM toimprove WHERE id='$id' ");
		$resultToImprove = array();
		while($row = $stmtToImprove->fetch_assoc())
		{
			array_push($resultToImprove, $row);
		}
		$oneMessage = array();
		array_push($oneMessage, $resultToImprove);
		break;
	case 2:
		$stmtWentWell = $Database->query("SELECT id, text, reporter FROM wentwell WHERE id='$id' ");
		$resultWentWell = array();
		while($row = $stmtWentWell->fetch_assoc())
		{
			array_push($resultWentWell, $row);
		}
		$oneMessage = array();
		array_push($oneMessage, $resultWentWell);
		break;
}

$Database->close();

echo json_encode($oneMessage);