<?php
header("Access-Control-Allow-Origin: *");

require_once ('init.php');
require_once ('token.php');
require_once ('check.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$email = $request->email;
$pass = $request->pass;


	$stmt = $Database->query(
		sprintf("SELECT * FROM users WHERE email='%s' AND password='%s'",
		mysqli_real_escape_string($Database, $email),
		mysqli_real_escape_string($Database, $pass)));

	if($stmt->num_rows>0) {
		$loggedUser = $stmt->fetch_assoc();
		$id = $loggedUser['id'];
		$userName = $loggedUser['name'];
		$role = $loggedUser['role'];
		$response = new Check(true, $id, $userName, $token, $role);
	}
	else
	{
		$response = new Check(false, "", "", "", "");
	}

echo json_encode($response);
