<?php

require_once ('init.php');
require_once ('check.php');
require_once ('validation.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);


$name = $request->name;
$email = $request->email;
$pass = $request->pass;
$passconf = $request->passconf;
$role = $request->role;

$errorName = '';
$errorEmail = '';
$errorPass = '';
$errorPassconf = '';

// ====================== WALIDACJA =============================

	$set = true;

	if((strlen($name)<3) || (strlen($name)>15))
	{
		$set = false;
		$errorName = "To pole może posiadać od 3 do 15 znaków";
	};

	$emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

	if((filter_var($emailB, FILTER_VALIDATE_EMAIL)==false) || ($emailB!=$email))
	{
		$set = false;
		$errorEmail = "Podaj poprawny adres e-mail";
	};


	$stmt = $Database->query(
		sprintf("SELECT id FROM users WHERE email='%s' ",
		mysqli_real_escape_string($Database, $email)));
	if($stmt->num_rows>0) 
	{
		$set = false;
		$errorEmail = "Istnieje już w bazie taki e-mail";
	}

	if((strlen($pass))<7 || (strlen($pass))>20)
	{
		$set = false;
		$errorPass = "Hasło musi posiadać od 7 do 20 znaków";
	};
	
	if($pass != $passconf)
	{
		$set = false;
		$errorPassconf = "Podane hasła nie są identyczne";
	};
	


// koncowy warunek
	if($set == true)
	{
		$Database->query(
			sprintf("INSERT INTO users (name, email, password, passconf, role) VALUES ('%s', '%s', '%s', '%s', '%s')",
			mysqli_real_escape_string($Database, $name),
			mysqli_real_escape_string($Database, $email),
			mysqli_real_escape_string($Database, $pass),
			mysqli_real_escape_string($Database, $passconf),
			mysqli_real_escape_string($Database, $role)));

		$response = new Validation(true, "", "", "", "");

	}
	else
	{
		$response = new Validation(false, $errorName, $errorEmail, $errorPass, $errorPassconf);
	};


echo json_encode($response);
