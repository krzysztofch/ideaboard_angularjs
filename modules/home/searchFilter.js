'use strict';

angular.module('Home').filter('searchFilter',['$rootScope', function ($rootScope) {
    return function (stickies) {
      var filtered = stickies.filter(function(element){          
          if(element.text.toLowerCase().indexOf($rootScope.searchFilter.toLowerCase()) != -1 || $rootScope.searchFilter == "") {
              return true;
          } else {
              return false;
          }
      });
      
      return filtered;
    };
  
}]);