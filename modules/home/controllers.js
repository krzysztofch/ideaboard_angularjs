'use strict';

angular.module('Home')

.controller('HomeController' , [ '$scope' , '$rootScope' , '$location' , '$routeParams' , 'MainService' , 'Database', 'DataTrans' , function( $scope, $rootScope, $location, $routeParams , MainService, Database, DataTrans ) {

	$scope.isLoading = true;
	$scope.currentUserID = MainService.getUserId();
	$scope.currentUser = MainService.getUser();
	$scope.role = MainService.getUserRole();
	$scope.showPicture = true;

	$scope.toDo = [];
	$scope.toImprove = [];
	$scope.wentWell = [];
	$scope.messageToAddTable;
    $scope.messageToAddTableName;
    $scope.numberTable;
    $scope.message = "";
	$scope.maxCharacters = 110;
	$scope.leftCharacters = $scope.maxCharacters;
	$scope.id = '';	
	$scope.tableNumber = '';
	$scope.text='';

	$scope.adjustHeight = function () {
		var maxHeight = 0;
		//timeout - wait till angular complete rendering
		setTimeout(function () {
			$('.dropable').each(function(index){
				if($(this).height() > maxHeight) {
					console.log('ustawiamy');
					maxHeight = $(this).height();
				}
			});

			$('.dropable').each(function(index){
				$(this).height(maxHeight);
			});

		},300);	
	}

    $scope.getMessages = function() {
    Database.getData().then(function successCallback(response) {
			$scope.isLoading = false;
			$scope.toDo = response.data[0];
			console.log(response.data[0]);
		    $scope.toImprove =  response.data[1];
			$scope.wentWell =  response.data[2];
			$scope.adjustHeight();
	    }).catch(function errorCallback(response) {
	          console.log("Unable to get records");
	    });	
    };    

    $scope.condition = function(table) {
    	$scope.messageToAddTable = table;

		switch($scope.messageToAddTable) {
			case 0: $scope.messageToAddTableName = 'toDo';
				break;
			case 1: $scope.messageToAddTableName = 'toImprove';
				break;
			case 2: $scope.messageToAddTableName = 'wentWell';
				break;
		} 
    };

    $scope.setTable = function(table) {
    	$scope.tableName = table;

    	switch($scope.tableName) {
    		case 'toDo': $scope.numberTable = 0;
    				break;
    		case 'toImprove': $scope.numberTable = 1;
    				break;
      		case 'wentWell': $scope.numberTable = 2;
    				break;
    	}
    	console.log($scope.numberTable);
    };

	$scope.addMessage = function() {
		Database.addData($scope.message, $scope.currentUserID, $scope.messageToAddTable).then(function(response){
			$scope.toDo = response.data[0];
			$scope.toImprove = response.data[1];
			$scope.wentWell = response.data[2];
			$scope.getMessages();
		});
		$scope.message = "";
        $('#notesModal').modal('hide');
	};

	$scope.removeMessage = function(table, item) {
		var tableNumber;

		switch(table) {
			case 'toDo': tableNumber = 0;
			break;			
			case 'toImprove': tableNumber = 1;
			break;			
			case 'wentWell': tableNumber = 2;
			break;
		}
        
		Database.removeData(tableNumber, item.id).then(function (response) {
				$scope.toDo =  response.data[0];
				$scope.toImprove =  response.data[1];
				$scope.wentWell =  response.data[2];
				// refresh the message list
				$scope.getMessages();
				console.log('Remove note');
		});
	};	

	$scope.readOneItem = function(table, item) {		
		$scope.text = item.text; 
		$scope.id = item.id;

		var tableNumber;

		switch(table) {
			case 'toDo': tableNumber = 0;
			break;			
			case 'toImprove': tableNumber = 1;
			break;			
			case 'wentWell': tableNumber = 2;
			break;
		}
		
		$scope.tableNumber = tableNumber;
	};

	$scope.updateMessage = function() {
		Database.updateMessage($scope.tableNumber, $scope.id, $scope.text).then(function successCallback(response) {
		    // refresh the message list
        	$scope.getMessages();
        	console.log($scope.currentUserID);
        	console.log($scope.response.reporter);
		}).catch(function errorCallback(response) {
			console.log("Unable to update record.");
		});

		$scope.text = "";
        $('#updateModal').modal('hide');
	};

	//load messages on start
	$scope.getMessages();

	// CHECK LEFT CHARACTERS IN ADD MODAL	
	$scope.$watch('message', function () {		
		if($scope.message.length > 110) {
			$scope.message = $scope.message.substring(0, 110);
		} else {
			$scope.leftCharacters = $scope.maxCharacters - $scope.message.length;
		}				             
    });

    // CHECK LEFT CHARACTERS IN UPDATE MODAL
	$scope.$watch('text', function () {		
		if($scope.text.length > 110) {
			$scope.text = $scope.text.substring(0, 110);
		} else {
			$scope.leftCharacters = $scope.maxCharacters - $scope.text.length;
		}				             
	});

	$scope.$watch('searchFilter', function () {		
		$rootScope.searchFilter = $scope.searchFilter;			             
	});

	//adjust column height
	$scope.$watchGroup(['toDo','toImprove','wentWell'], function (){	
		if (!$scope.isLoading){
			$scope.adjustHeight();
		}			
	});

	$scope.hiddenPicture = function() {
		$scope.showPicture = false;
	};

    $scope.handleDrop = function () {
      var movedDataObject = DataTrans.getMoved();
      $scope.setTable(movedDataObject.fromTable);
      var fromTable = $scope.numberTable;
      $scope.setTable(movedDataObject.toTable);
      var toTable = $scope.numberTable;

      console.log('handleDrop');
      console.log(movedDataObject);
      console.log(fromTable);
      console.log(toTable);

      Database.moveData(fromTable, toTable, movedDataObject.id, movedDataObject.text, movedDataObject.reporter).then(function (response) {
        $scope.getMessages();
      });
    };

}]);