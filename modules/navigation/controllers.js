'use strict';

var myCtrl = angular.module('Navigation');

myCtrl.controller('NavigationController' , [ '$scope' , '$rootScope' , '$location', 'MainService' , '$cookies', '$timeout' , function( $scope, $rootScope, $location, MainService, $cookies, $timeout ) {

	$scope.userEmail = MainService.getUserEmail();
	$scope.updatePass = '';

	if(MainService.isUserAuthenticated()) {
		$location.path('/home');
	};


	$scope.navigation = function() {
		return 'modules/navigation/views/navigation.html';
	};

	$scope.$watch( function () {
		if($location.path() === '/navigation') {
			$scope.tlo = true;
		} else {
			$scope.tlo = false;
		}
	});

	$scope.$watch( function (){
		
		if($location.path() === '/home' || $location.path() === '/register')	
			{
				$scope.success = true;
				$scope.currentUserRole = MainService.getUserRole();
				if($scope.currentUserRole == 'admin')
				{
					$scope.role = true;
				} else {
					$scope.role = false;
				}
			}
		else
			$scope.success = false;
	});

	$scope.isActive = function( path ) { 
	  return $location.path() === path;     
	};


	$scope.logout = function() {
		MainService.clearCredentials();
		$location.path('/navigation');

	};


	$scope.show = function() {
		$('#update').modal('show');
	};


	$scope.updatePassword = function() {
		MainService.updatePassword($scope.userEmail, $scope.updatePass).then(function successCallback(response) {
		    $scope.response = response.data;
			if($scope.response.success) {
				$scope.message = $scope.response.message;
				$timeout(function(){
					$('#update').modal('hide');
				}, 2000 );
			} else {
				$scope.error = $scope.response.message;
			}
		}).catch(function errorCallback(response) {
			console.log("Unable to update login.");
		});
	};

}]);



myCtrl.controller('LoginController' , [ '$scope' , '$rootScope' , '$location', 'MainService' , '$cookies', function( $scope, $rootScope, $location, MainService, $cookies ) {


	if(MainService.isUserAuthenticated()) {
		$location.path('/home');
	};

	// $scope.$watch( function () {
	// 	if($location.path() === '/login') {
	// 		$scope.tlo = false;
	// 		console.log('tlo-false');
	// 	}
	// });

	$scope.login = function() {
		MainService.Login($scope.userEmail, $scope.userPassword, function(response) {
			$scope.response = response.data;
			if($scope.response.success) {
				MainService.setCredentials($scope.userEmail, response.data.userName, response.data.id, response.data.role);
				MainService.setCookie($scope.userEmail, $scope.response.token, $scope.response.userName, $scope.response.id, $scope.response.role);
				$location.path('/home');
			} else {
				$scope.error = $scope.response.message;
			}
		});
	};
}]);



myCtrl.controller('RegisterController' , [ '$scope' , '$rootScope' , '$location', 'MainService' , '$timeout' , function( $scope, $rootScope, $location, MainService, $timeout ) {

	// if(MainService.isUserAuthenticated()) {
	// 	$location.path('/home');
	// };

	$scope.register = function() {
		MainService.Register($scope.userName, $scope.userEmail, $scope.userPassword, $scope.userPassconf , function(response) {
			$scope.response = response.data;
			if($scope.response.success) {
				$scope.registerSuccess = true;

				$timeout(function(){
				$scope.userName = "";
				$scope.userEmail = "";
				$scope.userPassword = "";
				$scope.userPassconf = "";
			} , 3000 );
				
				console.log('Udało sie zapisać');
			} else {
				// $scope.error = true;
				$scope.errorName = $scope.response.errorName;
				$scope.errorEmail = $scope.response.errorEmail;
				$scope.errorPass = $scope.response.errorPass;
				$scope.errorPassconf = $scope.response.errorPassconf;
				console.log('Nie udało sie zapisać');
				console.log($scope.response);
			}
		});
	};
}]);