-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 25 Sty 2018, 19:54
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `ideaboardz`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `todo`
--

CREATE TABLE `todo` (
  `id` int(11) NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  `reporter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `todo`
--

INSERT INTO `todo` (`id`, `text`, `reporter`) VALUES
(6, 'update update update', 1),
(26, 'filtrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltrfiltr', 17),
(27, 'konstantynopolitańczykiewiczównakonstantynopolitańczykiewiczównakonstantynopolitańczykiewiczównakonstantynopol', 17),
(38, 'to do', 1),
(39, 'karteczka', 1),
(59, 'Wprowadź notatkę', 10),
(62, 'This is message from Magdalena', 22),
(66, 'Julia', 12),
(67, 'notatka Dyzia', 19),
(73, 'notatka Franka', 20);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `toimprove`
--

CREATE TABLE `toimprove` (
  `id` int(11) NOT NULL,
  `text` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `reporter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `toimprove`
--

INSERT INTO `toimprove` (`id`, `text`, `reporter`) VALUES
(2, 'egzamin update', 1),
(9, 'To jest test. Do końca kursu został miesiąc. Trzeba zrobić projekt !!!', 1),
(10, 'fgggh   hgn gfhgjg    hjhn     n hgjhkg hkmh j  j j h hg hg hgghs   sd dfvfd frtyiulo dvgghjhjl vbncxd eett ty', 1),
(11, 'test', 1),
(12, 'karteczka', 1),
(13, 'lalala to jest tekst na 100 znakow lalala lala to jest tekst na 100 znakow lalala to jest tekst na 100 znakow.', 1),
(17, '<script src=\"https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js\">Kliknij tutaj</script>', 17),
(18, 'To jest test. Do końca kursu został miesiąc. Trzeba zrobić projekt !!!', 1),
(19, 'Magdalena', 22);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `passconf` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `passconf`, `role`) VALUES
(1, 'Admin', 'admin@gmail.com', 'admin01', 'admin01', 'admin'),
(10, 'Pablo', 'pablo@gmail.com', 'uytrewq', 'uytrewq', 'user'),
(11, 'Krysia', 'krysia@gmail.com', 'asdfghj', 'asdfghj', 'user'),
(12, 'Julia', 'testerek@test.pl', 'lkjhgfd', 'lkjhgfd', 'user'),
(13, 'Helena', 'helena@gmail.com', 'gfdsahj', 'gfdsahj', 'user'),
(14, 'Jan', 'testowy@ytest.pl', 'ygvtfcr', 'ygvtfcr', 'user'),
(15, 'Gosia', 'gosia@gmail.com', 'gosiagosia', 'gosiagosia', 'user'),
(16, 'Elżbieta', 'ela@gmail.com', 'asdfghj', 'asdfghj', 'user'),
(17, 'Marcin', 'marcin@gmail.com', 'marcinmarcin', 'marcinmarcin', 'user'),
(18, 'Paweł', 'pawel@gmail.com', 'pawelpawel', 'pawelpawel', 'user'),
(19, 'Dyzio', 'dyzio@gmail.com', 'qwertyu', 'qwertyu', 'user'),
(20, 'Franek', 'franek@gmail.com', 'lkjhgfd', 'lkjhgfd', 'user'),
(21, 'Stasia', 'stasia@gmail.com', 'stasia4', 'stasia4', 'user'),
(22, 'Magdalena', 'magda@gmail.com', 'mnbvcxz', 'mnbvcxz', 'user');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wentwell`
--

CREATE TABLE `wentwell` (
  `id` int(11) NOT NULL,
  `text` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `reporter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `wentwell`
--

INSERT INTO `wentwell` (`id`, `text`, `reporter`) VALUES
(2, 'test test test testowy test update message', 1),
(3, 'Test message to jest update test message', 1),
(4, 'środa środa środa środa środa środa środa środa środa', 1),
(5, 'Wprowadź notatkę', 10),
(11, 'This is Pablo message ', 10),
(22, 'fgggh   hgn gfhgjg    hjhn     n hgjhkg hkmh j  j j h hg hg hgghs   sd dfvfd frtyiulo dvgghjhjl vbncxd eett ty', 1),
(23, 'to do to do', 1),
(24, 'Wprowadź notatkę', 10),
(27, 'notatka Stasi', 21),
(28, 'test test test2 2 2 2 lalalalalalalala', 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `todo`
--
ALTER TABLE `todo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toimprove`
--
ALTER TABLE `toimprove`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wentwell`
--
ALTER TABLE `wentwell`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `todo`
--
ALTER TABLE `todo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT dla tabeli `toimprove`
--
ALTER TABLE `toimprove`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT dla tabeli `wentwell`
--
ALTER TABLE `wentwell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
